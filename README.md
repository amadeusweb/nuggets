# Nuggets - "Propel your goals" #
## by organizing ideas, information, and tasks. ##

Showcased at [YieldMore](https://yieldmore.org/apps/nuggets/), this app will engage the registered user with the content, calls to action (in the real world) and goals of the website / each article.

Individuals will further be able to build their own tasklist / planner and take notes on any webpage.

Loosely based on a [bookmark manager](https://bookmarkos.com/every-bookmark-manager-ever-made/) design and appearing like sticky notes, this powerful homepage will look a little like the windows start screen.

When used on organization intranet websites, their goals will be able to percolate and shape individual action.

For now, its planned as a VS Code Plugin which is a mix of
 * Bookmark Manager
 * Mind Map Software
 * TODO List (Abstract Spoon Inspired)
 * Inbox / Calendar

Multiple Data Source Workspaces (tdl files from abstractspoon.com)

Sample Workspace in ./Workspaces/YMSample/

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How to run vscode extension? ###
cd /vscodext/nuggets
npm install
code .
F5 (or Run->Start Debugging)

Alt+Shift+P
type *Nuggets*
